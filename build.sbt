name := "sbt-multi-module-project"

scalaVersion := "2.11.8"

val commonSettings = Seq(version := buildVersion, scalaVersion := "2.11.8", organization := "me.prassee")
val root = (project in file(".")).aggregate(commons, finagle)

lazy val commons = (project in file("commons"))
  .settings(
    commonSettings
  )

lazy val finagle = (project in file("finagle"))
  .settings(
    commonSettings
  )
  .dependsOn(commons)
